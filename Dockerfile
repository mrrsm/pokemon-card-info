FROM golang:1.20-alpine

WORKDIR /app


COPY go.mod ./
# If we were using external modules we would want to download them to prep the env
# COPY go.sum ./
# RUN go mod download

# Move our files into the working directory
COPY *.go ./

# Build our binary to the root folder
RUN go build -o /pokemon_card_info

# Our default command will run the script with no inputs
CMD [ "/pokemon_card_info" ]

