package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

// BuildQuery returns the pokemon api query string
func BuildQuery(types []string, hp int, rarity string) string {
	var queryTypes = []string{}

	for _, t := range types {
		queryTypes = append(queryTypes, fmt.Sprintf("types:%s", t))
	}

	return fmt.Sprintf("%s AND hp:[%d TO *] AND rarity:%s", strings.Join(queryTypes, " OR "), hp, rarity)
}

func main() {
	// Allow overriding of arguments
	hp := flag.Int("hp", 90, "The minimum HP of the Pokemon")
	limit := flag.Int("limit", 10, "The number of results to return")
	page := flag.Int("page", 1, "The results page number")
	rarity := flag.String("rarity", "Rare", "The rarity of the Pokemon")
	types := flag.String("types", "fire,grass", "A comma separated list of Pokemon types")
	flag.Parse()

	// Convert our comma separated types to an array of types
	var parsedTypes = strings.Split(*types, ",")

	// Build our query string for the search parameter
	query := BuildQuery(parsedTypes, *hp, *rarity)

	// Get the cards based on the parameters provided (or the defaults)
	cards, err := GetCards(query, *page, *limit, "id", "id,name,types,hp,rarity")
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}

	// Build out output list in the format requested
	cardList := struct {
		Cards []*PokemonCardOutput `json:"Cards"`
	}{cards}

	// Convert our cardList into JSON
	dat, err := json.MarshalIndent(cardList, "", "    ")
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}

	// Handle the output, in this case we are just printing it to stdout
	fmt.Println(string(dat))
}
