# Pokemon Card Info

## Building

### Local

Building locally requires that you have a working golang environment setup
```
go build -o pokemon_card_info
```

### Docker

You can also build this via docker using the included Dockerfile
```
docker build . -t pokemon_card_info
```

## Running

### Local binary

If you built the binary via the local method above you can run the binary
```
./pokemon_card_info
```

If you want to see options you can override and/or see the defaults you can run
```
./pokemon_card_info -h
```

### Docker

If you built the docker image from above you can run the container to get results from all the defaults
```
docker run --rm pokemon_card_info
```

If you want to see options you can override and/or see the defaults you can run
```
docker run --rm pokemon_card_info /pokemon_card_info -h
```