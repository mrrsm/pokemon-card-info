package main

import (
	"encoding/json"
	"net/http"
	"strconv"
)

// A lighter version of the full PokemonCard struct represents the data we care about from the api.
type PokemonCardOutput struct {
	ID     string   `json:"id"`
	Hp     string   `json:"hp"`
	Name   string   `json:"name"`
	Rarity string   `json:"rarity"`
	Types  []string `json:"types"`
}

func GetCards(query string, page int, pageSize int, orderBy string, sel string) ([]*PokemonCardOutput, error) {
	req, err := http.NewRequest("GET", "https://api.pokemontcg.io/v2/cards", nil)
	if err != nil {
		return nil, err
	}

	// Build the query string
	q := req.URL.Query()
	q.Add("q", query)
	q.Add("page", strconv.Itoa(page))
	q.Add("pageSize", strconv.Itoa(pageSize))
	q.Add("orderBy", orderBy)
	q.Add("select", sel)
	req.URL.RawQuery = q.Encode()

	// Make the request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	// Receive the data and store it in our struct so we can return it
	defer res.Body.Close()
	cardList := struct {
		Cards []*PokemonCardOutput `json:"data"`
	}{}
	if err := json.NewDecoder(res.Body).Decode(&cardList); err != nil {
		return nil, err
	}

	return cardList.Cards, nil
}
